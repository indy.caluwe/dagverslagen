# StageVerslagen

Hierin zitten al mijn dagverslagen.
Alle notities van de dagen heb ik opgeslagen op OneDrive.
https://onedrive.live.com/about/nl-be/


# 2018-10-01
Voor mijn eerste dag was ik heel enthousiast omdat ik niet wist wat mij te wachten stond.
Maar ik werd hartelijk ontvangen en ik kreeg direct een rondleiding van het hele gebouw.
Na de rondleiding werd ik naar de IT afdeling gebracht, waar ik kennis maakte met mijn collega's/ mentoren.
Een van mijn monitoren heeft via Active Directory een persoonlijk account voor mij gemaakt op het bedrijfsnetwerk.
Hij heeft ineens wat uitleg gegeven over hun netwerk, Ze gebruiken een online netwerk dat werkt via de Cloud van Microsoft.
Ik kreeg daarna al direct de taak om in de database alle mensen die geen verjaardag online hebben gezet in een excel bestand te zetten.
En ook de taak om alle nummers in de database internationaal te maken. Ik vond het heel aangenaam dat ik al direct mee mocht helpen in het bedrijf.
daarna werd ik al direct meegevraagd om mee naar een meeting in de Geronika te gaan, de meeting ging over de aankoop van een digibord in de vergaderzaal.
ik heb in de meeting niet veel mee gesproken maar heb toch mee gepraat over het onderwerp en een goede input meegegeven.

bezochte bedrijven: https://www.geronika.be/

# 2018-10-02
Bij mijn tweede dag voelde ik me al meer op mijn gemak in de werkomgeving.
Ik leerde de monitoren beter kennen en mocht vaak mee op stap.
Samen met mijn monitor heb ik veel foto's gemaakt van het gebouw en de omgeving.
In de ochtend heb ik vooral aan mijn stage taken gewerkt omdat mijn hoofdmonitor afwezig was.
Het bedrijf gaf mij na de middag een taak om door hun database te zoeken naar ontbrekende verjaardagen.
Ik heb al de namen gedocumenteerd in een excel bestand en nadat ik klaar was gedeeld in onedrive.
De taak was heel vermoeiend omdat het een database van meer dan 725 namen en elke naam moest je apart nakijken.
Maar ik vond het ook wel fijn dat ik het bedrijf mee kon helpen.

# 2018-10-03
Op mijn derde dag mocht ik veel meehelpen met taken uitvoeren.
Ik vond dit heel fijn maar ook heel vermoeiend omdat het 4 uur lang gefocust werken was.
Mijn monitor heeft samen met mij verschillende taken uitgevoerd.
Zoals de macro's van excel voor de afdelng boekhouden upgraden.
Ik heb ook een meeting bijgewoond over een probleem met windows.
Ik kon niet altijd volgen met het gesprek maar heb toch veel notities gemaakt.

# 2018-10-04
Vandaag is mijn voorlaaste dag in het bedrijf en ik vind het oprecth jammer dat de stage gedaan is.
Ik heb zoveel bij geleerd deze dagen en ben gelukkig dat mijn monitoren mij steunen en helpen waar ze kunnen.
Het was een productieve dag ik heb geprobeerd om een laptop zijn bios te reparen.
Ik heb het probleem niet kunnen oplossen maar wel kunnen vinden.
Mijn monitoren hebben mij vandaag laten zien hoe je een switch beheerd en instaleert.


# 2018-10-05
Vandaag is het zo ver het is mijn laaste dag in het bedrijf.
Ik heb heel de dag een sober gevoel gehad en probeerde zoveel mogelijk te genieten van de tijd die ik nog had.
Mijn monitoren hebben speciaal voor mij een afsraak geregeld met hun 3D tekenenaars om mij bijles te geven.
Ik vond de bijles echt geweldig en vind het kei lief dat Sam die moeite voor mij heeft gedaan.
Voordat ik weg ging heb ik mijn monitoren bedankt voor hun tijd en moeite en gaf hun een pakje mercikes.






